A)	**Installation**

    1. Clone project https://anedovba@bitbucket.org/anedovba/test-app.git
    2. Go to the project's folder
    3. Run composer install
    4. Create a new DB in MySql
    5. Copy .env.example into .env
    6. Update DB connection setting in the .env file :     
        DB_CONNECTION=mysql
        DB_HOST=127.0.0.1
        DB_PORT=3306
        DB_DATABASE=homestead
        DB_USERNAME=homestead
        DB_PASSWORD=secret
    
    7. Run migrations and add DB seeds: 
       php artisan migrate
       php artisan db:seed
    
    8. Run php artisan key:generate
    
    9. Run the project in LAMP environment or with Local Development Server. php artisan serve - this command will start a development server at http://localhost:8000
    
    10. The conversion chart is stored in the billing_odds table, users inputs are stored in the log table.

B)	**About application**

    1.	User should register to see the modal popup.
    2.	On homepage click a button “Convert betting odds” 
    3.	Then choose betting odds type and value.
