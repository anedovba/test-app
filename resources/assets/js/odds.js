//get odds in modal popup and save users uncludes in log

$(function () {

    $("#type_select").change(function () {

        var type_odd = $(this).val();
        if (type_odd != 'Choose betting odds type') {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.post('/api/get-odds', function (response) {
                var odds = response;
                $('#value_select').children('option:not(:first)').remove();
                for (var key in odds) {

                    $("#value_select").append('<option value=' + odds[key]['id'] + '>' + odds[key][type_odd] + '</option>');
                    $("#value_select").show();

                }

                $('#value_select').change(function () {

                    var odd_id = $(this).val();
                    var key = odd_id - 1;

                    $('#td_uk').empty();
                    $('#td_uk').append(odds[key]['uk']);
                    $('#td_eu').empty();
                    $('#td_eu').append(odds[key]['eu']);
                    $('#td_us').empty();
                    $('#td_us').append(odds[key]['us']);

                    var user_id = $('#u_id').val();

                    $.post('/api/log', {
                        'user_id': user_id,
                        'type_odd': type_odd,
                        'billing_odd_id': odd_id
                    }, function (request) {
                    })

                });
            });

        }

        $("#value_select").hide();

    });
});