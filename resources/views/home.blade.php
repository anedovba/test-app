@extends('layouts.app')

@section('content')
<div class="container">


    <div class="row">

        <div class="col-md-8 col-md-offset-2">

            <div class="panel panel-default">

                <div class="panel-heading">Home Page</div>

                <div class="panel-body text-center">

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                   <h2>Welcome!</h2>

                        <!-- Button trigger modal -->
                    <button type="button" class="btn text-uppercase" data-toggle="modal" data-target="#exampleModalCenter">
                        convert betting odds
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >

                        <div class="modal-dialog modal-dialog-centered" role="document">

                            <div class="modal-content-custom">

                                <div class="modal-header">
                                    Choose odd
                                </div>

                                <div class="modal-body">

                                    <div class="row">

                                        <div class="col-md-6">

                                            <select class="custom-select" id="type_select">

                                                <option name="type" selected>Choose betting odds type</option>

                                                <option value="uk">Fractional Odds (UK)</option>
                                                <option value="eu">Decimal Odds (EU)</option>
                                                <option value="us">Moneyline Odds (USA)</option>

                                            </select>

                                        </div>

                                        <div class="col-md-6">

                                            <select class="custom-select" id="value_select" style="display: none">

                                                <option name="odds_value">Choose betting odds value</option>

                                            </select>

                                        </div>

                                    </div>

                                    <br>

                                    <div class="row">

                                        <table class="table-bordered table-responsive col-md-12">

                                            <thead>

                                            <tr>

                                                <th class="col-md-4 text-center">Fractional Odds (UK)</th>
                                                <th class="col-md-4 text-center">Decimal Odds (EU)</th>
                                                <th class="col-md-4 text-center">Moneyline Odds (USA)</th>
                                            </tr>

                                            </thead>

                                            <tbody>

                                            <tr>
                                                <td id="td_uk"></td>
                                                <td id="td_eu"></td>
                                                <td id="td_us"></td>
                                            </tr>

                                            </tbody>

                                        </table>

                                    </div>

                                    <input id="u_id" type="hidden" value="{{ $user_id }}">

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

@endsection

