<?php

namespace App\Http\Controllers\API;

use App\BettingOdd;
use App\Log;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class AppController extends Controller
{
    /**
     * Get the betting odds.
     *
     * @return Response
     */
    public function getOdds()
    {
        $odds = BettingOdd::all();
        return $odds;
    }

    /**
     * Store users inputs in the log table.
     *
     * @param Request $request
     * @return void
     */
    public function log(Request $request)
    {
        $data = $request->all();
        $log = new Log();
        $log->user_id = $data['user_id'];
        $log->type_odd = $data['type_odd'];
        $log->billing_odd_id = $data['billing_odd_id'];
        $log->save();
    }
}
